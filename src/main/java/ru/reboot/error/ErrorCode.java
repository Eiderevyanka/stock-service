package ru.reboot.error;

public class ErrorCode {
    public static final String ILLEGAL_ARGUMENT = "ILLEGAL_ARGUMENT";
    public static final String ITEM_NOT_FOUND = "ITEM_NOT_FOUND";
    public static final String ITEM_ALREADY_EXISTS = "ITEM_ALREADY_EXISTS";
    public static final String RESERVATION_ERROR = "RESERVATION_ERROR";
}
