package ru.reboot.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.reboot.dao.StockRepository;
import ru.reboot.dao.StockRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
public class Config {

    @Value("${jdbc.url}")
    private String url;

    @Bean
    public StockRepository stockRepositoryBean() throws SQLException {

        Connection connection = DriverManager.getConnection(url);
        return new StockRepositoryImpl(connection);
    }
}
