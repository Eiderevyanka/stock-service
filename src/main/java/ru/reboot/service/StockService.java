package ru.reboot.service;

import ru.reboot.dto.Item;

import java.util.List;

public interface StockService {

    /**
     * Получить информацию о товаре
     *
     * @throws ru.reboot.error.BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if itemId is empty or null
     */
    Item getItem(String itemId);

    /**
     * Получить товары в определенной категории
     *
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if category is empty or null
     */
    List<Item> getAllItemsByCategory(String category);

    /**
     * Удалить информацию о товаре
     *
     * @throws ru.reboot.error.BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if itemId is empty or null
     */
    void deleteItem(String itemId);

    /**
     * Создать новый товар
     *
     * @param item информация по сохраняемому товару
     * @return сохраненный товар
     * @throws ru.reboot.error.BusinessLogicException with code ITEM_ALREADY_EXISTS if item with id already exists
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if item contains bad data
     */
    Item createItem(Item item);

    /**
     * Обновить информацию о существующем товаре.
     *
     * @param item информация по обновляемому товару
     * @return обновлённый товар
     * @throws ru.reboot.error.BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if item contains bad data
     */
    Item updateItem(Item item);

    /**
     * Получить все товары
     */
    List<Item> getAllItems();

    /**
     * Получить все категории товаров
     */
    List<String> getAllCategories();

    /**
     * Забронировать определенное количество товара
     *
     * @throws ru.reboot.error.BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws ru.reboot.error.BusinessLogicException with code RESERVATION_ERROR if count > stock count
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if itemId or count are wrong
     */
    Item reserveItem(String itemId, int count);

    /**
     * Отменить бронь определенного количества товара
     *
     * @throws ru.reboot.error.BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws ru.reboot.error.BusinessLogicException with code RESERVATION_ERROR if final reserved count less than zero
     * @throws ru.reboot.error.BusinessLogicException with code ILLEGAL_ARGUMENT if itemId or count are wrong
     */
    Item unreserveItem(String itemId, int count);
}
