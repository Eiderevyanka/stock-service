package ru.reboot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.dto.ItemChangedEvent;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import java.util.List;
import java.util.Objects;

@Component
public class StockServiceImpl implements StockService {

    private static final Logger logger = LogManager.getLogger(StockServiceImpl.class);

    private StockRepository stockRepository;
    private ObjectMapper mapper;
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public void setStockRepository(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Autowired
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setKafkaTemplate(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     * Получить информацию о товаре
     *
     * @throws BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if itemId is empty or null
     */
    @Override
    public Item getItem(String itemId) throws BusinessLogicException {

        try {
            logger.info("Method .getItem started itemId={}", itemId);

            validateItemId(itemId, ".getItem");
            Item item = stockRepository.findItem(itemId);

            if (item == null) {
                throw new BusinessLogicException("Item itemId=" + itemId + " not found",
                        ErrorCode.ITEM_NOT_FOUND);
            }

            logger.info("Method .getItem completed itemId={}, item={}", itemId, item);
            return item;
        } catch (Exception e) {
            logger.error("Failed to .getItem error= " + e, e);
            throw e;
        }
    }

    /**
     * Получить товары в определенной категории
     *
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if category is empty or null
     */
    @Override
    public List<Item> getAllItems() throws BusinessLogicException {
        logger.info("Method .getAllItems");
        List<Item> allItems;
        try {
            allItems = stockRepository.getAllItems();
            logger.info("Method .getAllItems completed result={}", allItems);
            return allItems;
        } catch (BusinessLogicException e) {
            logger.error("Failed to .getAllItems error= " + e, e);
            throw e;
        }
    }

    /**
     * Получить все категории товаров
     *
     * @return List of Strings with all categories Items
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */
    @Override
    public List<String> getAllCategories() throws BusinessLogicException {

        logger.info("Method .getAllCategories");
        List<String> allCategories;
        try {
            allCategories = stockRepository.getAllCategories();
            logger.info("Method .getAllCategories completed result={}", allCategories);
            return allCategories;
        } catch (BusinessLogicException e) {
            logger.error("Method .getAllCategories threw BusinessLogicException with code=" + e.getCode());
            throw e;
        }
    }

    /**
     * Забронировать определенное количество товара
     *
     * @param itemId The Id  of the Item to reserve
     * @param count  The quantity  of the Item to reserve
     * @return reserved Item
     * @throws BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws BusinessLogicException with code RESERVATION_ERROR if count > stock count
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if count are wrong
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */
    @Override
    public Item reserveItem(String itemId, int count) throws BusinessLogicException {

        logger.info("Method .reserveItem itemId={} count={}", itemId, count);

        try {

            if (itemId == null || count <= 0) {
                throw new BusinessLogicException("itemId is null or count less than 1", ErrorCode.ILLEGAL_ARGUMENT);
            }

            Item foundItem = stockRepository.findItem(itemId);

            if (foundItem == null) {
                throw new BusinessLogicException("Item with Id=" + itemId + " not found", ErrorCode.ITEM_NOT_FOUND);
            }

            int foundItemCount = foundItem.getCount();
            int foundItemReserved = foundItem.getReserved();

            if (foundItemCount - foundItemReserved < count) {
                throw new BusinessLogicException("Item with Id=" + itemId + " is out of stock in the required quantity",
                        ErrorCode.RESERVATION_ERROR);
            }

            foundItem.setReserved(foundItemReserved + count);
            Item reservedItem = stockRepository.updateItem(foundItem);
            sendMessage(new ItemChangedEvent(ItemChangedEvent.Type.UPDATE, reservedItem));

            logger.info("Method .reserveItem completed itemId={} count={} result={}", itemId, count, reservedItem);
            return reservedItem;

        } catch (Exception e) {
            logger.error("Failed to .reserveItem error={}", e.toString(), e);
            throw e;
        }


    }

    /**
     * Отменить бронь определенного количества товара
     *
     * @param itemId Item Id for unreserve
     * @param count  Item quantity for unreserve
     * @return {@link Item} unreserved Item
     * @throws BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws BusinessLogicException with code RESERVATION_ERROR if count > stock count
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if count are wrong
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */
    @Override
    public Item unreserveItem(String itemId, int count) throws BusinessLogicException {
        logger.info("Method .unreserveItem itemId={} count={}", itemId, count);

        try {
            if (itemId == null || count <= 0) {
                throw new BusinessLogicException("itemId can't be null or count can't be less than 1",
                        ErrorCode.ILLEGAL_ARGUMENT);
            }

            Item foundItem = stockRepository.findItem(itemId);

            if (foundItem == null) {
                throw new BusinessLogicException("Item with Id= " + itemId + " not found", ErrorCode.ITEM_NOT_FOUND);
            }

            int foundItemReserved = foundItem.getReserved();
            if (foundItemReserved < count) {
                throw new BusinessLogicException("Item with Id= " + itemId +
                        " to unreserve quantity can't be more than reserved quantity", ErrorCode.RESERVATION_ERROR);
            }

            foundItem.setReserved(foundItemReserved - count);
            Item reservedItem = stockRepository.updateItem(foundItem);
            sendMessage(new ItemChangedEvent(ItemChangedEvent.Type.UPDATE, reservedItem));

            logger.info("Method .unreserveItem completed itemId={} count={} result={}",
                    itemId, count, reservedItem);
            return reservedItem;
        } catch (Exception e) {
            logger.error("Failed to .unreserveItem error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Получить все товары определенной категории из базы данных
     *
     * @param category
     * @return List<Item>
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if category is empty or null
     */
    @Override
    public List<Item> getAllItemsByCategory(String category) throws BusinessLogicException {
        logger.info("Method .getAllItemsByCategory category ={}", category);
        try {
            if (Objects.isNull(category)) {
                throw new BusinessLogicException("Category is null", ErrorCode.ILLEGAL_ARGUMENT);
            }
            List<Item> itemList;
            itemList = stockRepository.findAllItemsByCategory(category);
            logger.info("Method .getAllItemsByCategory completed category ={} itemList={}", category, itemList);
            return itemList;
        } catch (Exception ex) {
            logger.error("Method .getAllItemsByCategory threw BusinessLogicException with code={}", ex.toString(), ex);
            throw ex;
        }
    }


    /**
     * Удалить информацию о конкретном товаре из базы данных
     *
     * @param itemId
     * @throws BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if itemId is empty or null
     */
    @Override
    public void deleteItem(String itemId) throws BusinessLogicException {
        logger.info("Method .deleteItem itemId ={}", itemId);
        validateItemId(itemId, ".deleteItem");
        try {
            Item deletedItem = stockRepository.findItem(itemId);
            stockRepository.deleteItem(itemId);
            sendMessage(new ItemChangedEvent(ItemChangedEvent.Type.DELETE, deletedItem));
            logger.info("Method .deleteItem completed itemId ={}", itemId);
        } catch (Exception ex) {
            logger.error("Method .deleteItem threw BusinessLogicException with code={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * Создать новый товар
     *
     * @param item информация по сохраняемому товару
     * @return сохраненный товар
     * @throws BusinessLogicException with code ITEM_ALREADY_EXISTS if item with id already exists
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if item contains bad data
     */
    @Override
    public Item createItem(Item item) throws BusinessLogicException {
        logger.info("Method .createItem item=" + item);
        Item resultItem;

        try {
            validateItem(item, ".createItem");
            Item foundItem = stockRepository.findItem(item.getId());

            if (Objects.nonNull(foundItem)) {
                throw new BusinessLogicException("Item is already exists", ErrorCode.ITEM_ALREADY_EXISTS);
            }
            resultItem = stockRepository.createItem(item);
            sendMessage(new ItemChangedEvent(ItemChangedEvent.Type.UPDATE, resultItem));
        } catch (Exception ex) {
            logger.error("Failed to .createItem error={}", ex.toString(), ex);
            throw ex;
        }
        logger.info("Method .createItem completed item=" + item
                + "result=" + item);
        return resultItem;

    }

    /**
     * Обновить информацию о существующем товаре.
     *
     * @param item информация по обновляемому товару
     * @return обновлённый товар
     * @throws BusinessLogicException with code ITEM_NOT_FOUND if item doesn't exist
     * @throws BusinessLogicException with code ILLEGAL_ARGUMENT if item contains bad data
     */
    @Override
    public Item updateItem(Item item) throws BusinessLogicException {
        logger.info("Method .updateItem item=" + item);
        Item resultItem;

        try {
            validateItem(item, ".updateItem");
            Item foundItem = stockRepository.findItem(item.getId());

            if (Objects.isNull(foundItem)) {
                throw new BusinessLogicException("Item is not found", ErrorCode.ITEM_NOT_FOUND);
            }

            resultItem = stockRepository.updateItem(item);
            sendMessage(new ItemChangedEvent(ItemChangedEvent.Type.UPDATE, resultItem));
        } catch (Exception ex) {
            logger.error("Failed to .createItem error={}", ex.toString(), ex);
            throw ex;
        }
        logger.info("Method .updateItem completed item=" + item
                + "result=" + item);
        return resultItem;
    }


    private void validateItem(Item item, String methodName) throws BusinessLogicException {
        if (Objects.isNull(item)) {
            throw new BusinessLogicException("Item contains bad data", ErrorCode.ILLEGAL_ARGUMENT);
        }
        validateItemId(item.getId(), methodName);
    }

    private void validateItemId(String itemId, String methodName) throws BusinessLogicException {
        if (Objects.isNull(itemId) || itemId.equals("")) {
            throw new BusinessLogicException("ItemId is empty or null", ErrorCode.ILLEGAL_ARGUMENT);
        }
    }

    private void sendMessage(ItemChangedEvent itemChangedEvent) {
        try {
            logger.info(String.format("#### -> Price Service message -> %s", mapper.writeValueAsString(itemChangedEvent)));
            kafkaTemplate.send(ItemChangedEvent.TOPIC, mapper.writeValueAsString(itemChangedEvent));
            logger.info(">> Send: TOPIC={}, {}, eventType={}", ItemChangedEvent.TOPIC, itemChangedEvent, itemChangedEvent.getEventType());
        } catch (Exception e) {
            logger.error("Failed to .sendMessage error={}, eventType={}", e.toString(), itemChangedEvent.getEventType(), e);
        }
    }
}
