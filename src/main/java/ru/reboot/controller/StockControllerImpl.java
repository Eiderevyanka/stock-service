package ru.reboot.controller;

import org.apache.kafka.common.protocol.types.Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dao.StockRepositoryImpl;
import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.StockService;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Stock controller.
 */
@RestController
@RequestMapping(path = "stock")
public class StockControllerImpl implements StockController {

    private static final Logger logger = LogManager.getLogger(StockRepositoryImpl.class);

    private StockService stockService;

    @Autowired
    public void setStockService(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "StockController " + new Date();
    }

    /**
     * Получить все товары определенной категории
     *
     * @param categoryId
     * @return List<Item>
     */

    @Override
    @GetMapping(value = "/item/all/byCategory")
    public List<Item> getAllItemsByCategory(@RequestParam("categoryId") String categoryId) {
        return stockService.getAllItemsByCategory(categoryId);
    }

    /**
     * Получить информацию о товаре
     *
     * @param itemId
     * @return Item
     */
    @GetMapping(value = "/item")
    public Item getItem(@RequestParam("itemId") String itemId) {
        return stockService.getItem(itemId);
    }

    /**
     * Удалить информацию о конкретном товаре
     *
     * @param itemId
     */

    @Override
    @DeleteMapping(value = "/item")
    public void deleteItem(@RequestParam("itemId") String itemId) {
        stockService.deleteItem(itemId);
    }

    /**
     * Создать новый товар
     *
     * @param item информация по сохраняемому товару
     * @return сохраненный товар Item
     */
    @Override
    @PostMapping(value = "/item")
    public Item createItem(@RequestBody Item item) {
        return stockService.createItem(item);
    }

    /**
     * Обновить информацию о существующем товаре
     *
     * @param item информация по обновляемому товару
     * @return обновлённый товар
     */
    @Override
    @PutMapping(value = "/item")
    public Item updateItem(@RequestBody Item item) {
        return stockService.updateItem(item);
    }

    /**
     * Получить все товары
     *
     * @return List of {@link Item} with all Items or empty List
     */
    @Override
    @GetMapping("/item/all")
    public List<Item> getAllItems() {
        return stockService.getAllItems();
    }

    /**
     * Получить все категории товаров GET /stock/сategory/all
     *
     * @return List of Strings with all categories Items
     */
    @Override
    @GetMapping("/сategory/all")
    public List<String> getAllCategories() {
        return stockService.getAllCategories();
    }

    /**
     * Забронировать определенное количество товара POST /stock/reserve?itemId={itemId}&count={count}
     *
     * @return reserved Item
     */
    @Override
    @PostMapping(value = "/reserve")
    public Item reserveItem(@RequestParam("itemId") String itemId, @RequestParam("count") int count) {
        return stockService.reserveItem(itemId, count);
    }

    /**
     * Отменить бронь определенного количества товара
     *
     * @param itemId Item Id for unreserve
     * @param count  Item quantity for unreserve
     * @return {@link Item} unreserved Item
     */
    @Override
    @PostMapping("/unreserve")
    public Item unreserveItem(@RequestParam("itemId") String itemId, @RequestParam("count") int count) {
        return stockService.unreserveItem(itemId, count);
    }

    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<Map<String, String>> handleException(BusinessLogicException ex, HttpServletRequest request) {

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Map<String, String> map = new HashMap<>();

        map.put("errorCode", ex.getCode());
        map.put("message", ex.getMessage());
        map.put("timestamp", LocalDateTime.now().toString());
        map.put("status", String.valueOf(status.value()));
        map.put("error", status.getReasonPhrase());
        map.put("path", request.getRequestURI());

        return new ResponseEntity<>(map, status);
    }
}
