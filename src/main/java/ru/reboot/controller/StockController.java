package ru.reboot.controller;

import ru.reboot.dto.Item;

import java.util.List;

public interface StockController {

    /**
     * Получить информацию о товаре
     */
    Item getItem(String itemId);

    /**
     * Получить товары в определенной категории
     */
    List<Item> getAllItemsByCategory(String category);

    /**
     * Удалить информацию о товаре
     */
    void deleteItem(String itemId);

    /**
     * Создать новый товар
     * @param item информация по сохраняемому товару
     * @return сохраненный товар
     */
    Item createItem(Item item);

    /**
     * Обновить информацию о существующем товаре
     * @param item информация по обновляемому товару
     * @return обновлённый товар
     */
    Item updateItem(Item item);

    /**
     * Получить все товары
     */
    List<Item> getAllItems();

    /**
     * Получить все категории товаров
     */
    List<String> getAllCategories();

    /**
     * Забронировать определенное количество товара
     */
    Item reserveItem(String itemId, int count);

    /**
     * Отменить бронь определенного количества товара
     */
    Item unreserveItem(String itemId, int count);
}
