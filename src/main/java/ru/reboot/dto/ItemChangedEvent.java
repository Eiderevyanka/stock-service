package ru.reboot.dto;

/**
 * Price changed event.
 * Use in ITEM_CHANGED_EVENT kafka topic
 */
public class ItemChangedEvent {

    public static final String TOPIC = "ITEM_CHANGED_EVENT";

    /**
     * Event type
     */
    public static class Type {
        public static final String UPDATE = "UPDATE";
        public static final String DELETE = "DELETE";
    }

    private Item item;
    private String eventType;

    public ItemChangedEvent() {
    }

    public ItemChangedEvent(String eventType, Item item) {
        this.item = item;
        this.eventType = eventType;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "PriceChangedEvent{" +
                "eventType=" + eventType +
                " item=" + item +
                '}';
    }
}
