package ru.reboot.dto;

public class Item {

    private String id;
    private String name;
    private String shortName;
    private String description;
    private String unit;
    private double purchasePrice;
    private String category;
    private String manufacturer;
    private int count;
    private int reserved;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getReserved() {
        return reserved;
    }

    public void setReserved(int reserved) {
        this.reserved = reserved;
    }

    public static class Builder {

        private final Item newItem;

        public Builder() {
            this.newItem = new Item();
        }

        public Builder setId(String id) {
            newItem.id = id;
            return this;
        }

        public Builder setName(String name) {
            newItem.name = name;
            return this;
        }

        public Builder setShortName(String shortName) {
            newItem.shortName = shortName;
            return this;
        }

        public Builder setDescription(String description) {
            newItem.description = description;
            return this;
        }

        public Builder setUnit(String unit) {
            newItem.unit = unit;
            return this;
        }

        public Builder setPurchasePrice(double purchasePrice) {
            newItem.purchasePrice = purchasePrice;
            return this;
        }

        public Builder setCategory(String category) {
            newItem.category = category;
            return this;
        }

        public Builder setManufacturer(String manufacturer) {
            newItem.manufacturer = manufacturer;
            return this;
        }

        public Builder setCount(int count) {
            newItem.count = count;
            return this;
        }

        public Builder setReserved(int reserved) {
            newItem.reserved = reserved;
            return this;
        }

        public Item build() {
            return newItem;
        }
    }

    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", description='" + description + '\'' +
                ", unit='" + unit + '\'' +
                ", purchasePrice=" + purchasePrice +
                ", category='" + category + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", count=" + count +
                ", reserved=" + reserved +
                '}';
    }
}
