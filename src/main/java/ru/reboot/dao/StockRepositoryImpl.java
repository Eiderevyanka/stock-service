package ru.reboot.dao;

import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import javax.annotation.PreDestroy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Stock repository.
 */
public class StockRepositoryImpl implements StockRepository {

    private final Connection connection;

    public StockRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    /**
     * Получить информацию о товаре
     *
     * @return Item item
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */
    @Override
    public Item findItem(String itemId) {

        String query = "SELECT * FROM item WHERE id =?;";

        try (PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, itemId);

            try (ResultSet result = statement.executeQuery()) {

                if (result.next()) {

                    return new Item.Builder()
                            .setId(result.getString("id"))
                            .setName(result.getString("name"))
                            .setShortName(result.getString("short_name"))
                            .setDescription(result.getString("description"))
                            .setUnit(result.getString("unit"))
                            .setPurchasePrice(result.getDouble("purchase_price"))
                            .setCategory(result.getString("category"))
                            .setManufacturer(result.getString("manufacturer"))
                            .setCount(result.getInt("count"))
                            .setReserved(result.getInt("reserved"))
                            .build();
                }

            }

        } catch (SQLException throwables) {
            throw new BusinessLogicException(throwables.getMessage(), "DATABASE_ERROR");
        }
        return null;
    }

    /**
     * Получить все товары определенной категории из базы данных
     *
     * @return List<Item>
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */

    @Override
    public List<Item> findAllItemsByCategory(String category) throws BusinessLogicException {
        List<Item> itemList = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM item WHERE category =?;")) {
            statement.setString(1, category);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Item newItem = new Item.Builder().setId(result.getString("id"))
                            .setName(result.getString("name"))
                            .setShortName(result.getString("short_name"))
                            .setDescription(result.getString("description"))
                            .setUnit(result.getString("unit"))
                            .setPurchasePrice(result.getDouble("purchase_price"))
                            .setCategory(result.getString("category"))
                            .setManufacturer(result.getString("manufacturer"))
                            .setCount(result.getInt("count"))
                            .setReserved(result.getInt("reserved"))
                            .build();
                    itemList.add(newItem);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new BusinessLogicException(e.getMessage(), "DATABASE_ERROR");
        }
        return itemList;
    }

    /**
     * Удалить информацию о конкретном товаре из базы данных
     *
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */

    @Override
    public void deleteItem(String itemId) throws BusinessLogicException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM item WHERE id =?;")) {
            statement.setString(1, itemId);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new BusinessLogicException("Item ID=" + itemId + " not found in DB", ErrorCode.ITEM_NOT_FOUND);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new BusinessLogicException(e.getMessage(), "DATABASE_ERROR");
        }
    }

    /**
     * Создать новый товар
     *
     * @return Item
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */
    @Override
    public Item createItem(Item item) {
        String sql = "INSERT INTO item VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, item.getId());
            statement.setString(2, item.getName());
            statement.setString(3, item.getShortName());
            statement.setString(4, item.getDescription());
            statement.setString(5, item.getUnit());
            statement.setDouble(6, item.getPurchasePrice());
            statement.setString(7, item.getCategory());
            statement.setString(8, item.getManufacturer());
            statement.setInt(9, item.getCount());
            statement.setInt(10, item.getReserved());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new BusinessLogicException(e.getMessage(), "DATABASE_ERROR");
        }
        return item;
    }

    /**
     * Создать новый товар
     *
     * @return Item
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */
    @Override
    public Item updateItem(Item item) {
        String sql = "UPDATE item SET name=?, short_name=?, description=?, unit=?," +
                "purchase_price=?, category=?, manufacturer=?, count=?, reserved=?" +
                "WHERE id=?;";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, item.getName());
            statement.setString(2, item.getShortName());
            statement.setString(3, item.getDescription());
            statement.setString(4, item.getUnit());
            statement.setDouble(5, item.getPurchasePrice());
            statement.setString(6, item.getCategory());
            statement.setString(7, item.getManufacturer());
            statement.setInt(8, item.getCount());
            statement.setInt(9, item.getReserved());
            statement.setString(10, item.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new BusinessLogicException(e.getMessage(), "DATABASE_ERROR");
        }
        return item;
    }

    /**
     * Получить все товары
     *
     * @return List of {@link Item} with all Items or null
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */
    @Override
    public List<Item> getAllItems() {
        List<Item> items = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM item");
             ResultSet result = statement.executeQuery()) {

            while (result.next()) {
                items.add(new Item.Builder()
                        .setId(result.getString("id"))
                        .setName(result.getString("name"))
                        .setShortName(result.getString("short_name"))
                        .setDescription(result.getString("description"))
                        .setUnit(result.getString("unit"))
                        .setPurchasePrice(result.getDouble("purchase_price"))
                        .setCategory(result.getString("category"))
                        .setManufacturer(result.getString("manufacturer"))
                        .setCount(result.getInt("count"))
                        .setReserved(result.getInt("reserved"))
                        .build());
            }
        } catch (SQLException e) {
            throw new BusinessLogicException(e.getMessage(), "DATABASE_ERROR");
        }
        return items;
    }

    /**
     * Получить все категории товаров из базы данных
     *
     * @return List of Strings with all categories Items or null if no categories found
     * @throws BusinessLogicException with code DATABASE_ERROR if was SQLException
     */
    @Override
    public List<String> getAllCategories() throws BusinessLogicException {

        List<String> categories = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("SELECT DISTINCT category FROM item");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                categories.add(resultSet.getString("category"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new BusinessLogicException(e.getMessage(), "DATABASE_ERROR");
        }
        return categories;
    }

    @PreDestroy
    public void onClose() {
        try {
            connection.close();
        } catch (Exception ex) {
        }
    }
}
