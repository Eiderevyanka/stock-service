package ru.reboot.dao;

import ru.reboot.dto.Item;

import java.sql.SQLException;
import java.util.List;

public interface StockRepository {

    /**
     * Получить информацию о товаре
     */
    Item findItem(String itemId);

    /**
     * Получить товары в определенной категории
     */
    List<Item> findAllItemsByCategory(String category);

    /**
     * Удалить информацию о товаре
     */
    void deleteItem(String itemId);

    /**
     * Создать новый товар
     */
    Item createItem(Item item);

    /**
     * Обновить информацию о существующем товаре
     */
    Item updateItem(Item item);

    /**
     * Получить все товары
     */
    List<Item> getAllItems();

    /**
     * Получить все категории товаров
     */
    List<String> getAllCategories();
}
