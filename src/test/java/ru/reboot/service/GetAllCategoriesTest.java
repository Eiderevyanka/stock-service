package ru.reboot.service;


import junit.framework.TestCase;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.StockServiceImpl;

import java.util.Arrays;

public class GetAllCategoriesTest extends TestCase {

    @Test
    public void testGetAllCategories() {

        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        Mockito.when(repositoryMock.getAllCategories()).thenReturn(Arrays.asList("one", "two"));
        assertEquals(service.getAllCategories(), Arrays.asList("one", "two"));

        Mockito.when(repositoryMock.getAllCategories()).thenThrow(new BusinessLogicException("", "DATABASE_ERROR"));
        try {
            service.getAllCategories();
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), "DATABASE_ERROR");
        }
    }

}
