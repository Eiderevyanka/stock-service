package ru.reboot.service;

import junit.framework.TestCase;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.StockServiceImpl;

public class ReserveItemTest extends TestCase {

    private StockRepository repositoryMock = Mockito.mock(StockRepository.class);
    private StockServiceImpl service = new StockServiceImpl();
    private Item itemMock = Mockito.mock(Item.class);

    {
        service.setStockRepository(repositoryMock);
    }

    @Test
    public void testIllegalArgument() {

        try {
            service.reserveItem("testId", -1);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
        }
    }

    @Test
    public void testDatabaseErrorInFindItem() {

        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenThrow(new BusinessLogicException("", "DATABASE_ERROR"));
        try {
            service.reserveItem(Mockito.anyString(), 2);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), "DATABASE_ERROR");
            Mockito.verify(repositoryMock).findItem(Mockito.anyString());
        }

    }

    @Test
    public void testDatabaseErrorInUpdateItem() {

        Mockito.when(itemMock.getCount()).thenReturn(10);
        Mockito.when(itemMock.getReserved()).thenReturn(7);
        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(itemMock);
        Mockito.when(repositoryMock.updateItem(itemMock)).thenThrow(new BusinessLogicException("", "DATABASE_ERROR"));
        try {
            service.reserveItem(Mockito.anyString(), 2);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), "DATABASE_ERROR");
            Mockito.verify(repositoryMock).findItem(Mockito.anyString());
            Mockito.verify(repositoryMock).updateItem(itemMock);
        }
    }

    @Test
    public void testItemNotFound() {

        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(null);
        try {
            service.reserveItem(Mockito.anyString(), 2);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), "ITEM_NOT_FOUND");
            Mockito.verify(repositoryMock).findItem(Mockito.anyString());
        }
    }

    @Test
    public void testReservationError() {

        Mockito.when(itemMock.getCount()).thenReturn(10);
        Mockito.when(itemMock.getReserved()).thenReturn(9);
        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(itemMock);
        try {
            service.reserveItem(Mockito.anyString(), 2);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), "RESERVATION_ERROR");
            Mockito.verify(repositoryMock).findItem(Mockito.anyString());
            Mockito.verify(itemMock).getCount();
            Mockito.verify(itemMock).getReserved();
        }
    }

    @Test
    public void testReserveItemPositive() {

        Mockito.when(itemMock.getCount()).thenReturn(10);
        Mockito.when(itemMock.getReserved()).thenReturn(7);
        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(itemMock);

        Item updatedItemMock = Mockito.mock(Item.class);
        Mockito.when(updatedItemMock.getReserved()).thenReturn(9);
        Mockito.when(repositoryMock.updateItem(itemMock)).thenReturn(updatedItemMock);
        Item reservedItem = service.reserveItem(Mockito.anyString(), 2);

        assertSame(updatedItemMock, reservedItem);
        Mockito.verify(repositoryMock).findItem(Mockito.anyString());
        Mockito.verify(repositoryMock).updateItem(Mockito.any());
        Mockito.verify(itemMock).getCount();
        Mockito.verify(itemMock).getReserved();
        Mockito.verify(itemMock).setReserved(9);
    }
}