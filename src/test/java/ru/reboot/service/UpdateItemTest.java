package ru.reboot.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;
import ru.reboot.service.StockServiceImpl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UpdateItemTest {
    private StockRepository stockRepository = Mockito.mock(StockRepository.class);
    private StockServiceImpl stockService = new StockServiceImpl();
    private Item item;

    @Before
    public void init() {
        stockService.setStockRepository(stockRepository);

        item = new Item();
        item.setId("01");
        item.setName("First Item");
    }


    @Test
    public void updateItemNotFound() {
        when(stockRepository.findItem(item.getId())).thenReturn(null);

        try {
            stockService.updateItem(item);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCode.ITEM_NOT_FOUND, e.getCode());
        }
    }

    @Test
    public void updateItemContainsBadData() {
        try {
            stockService.updateItem(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCode.ILLEGAL_ARGUMENT, e.getCode());
        }
    }

    @Test
    public void updateItemPositive() {
        when(stockRepository.findItem(item.getId())).thenReturn(item);
        when(stockRepository.updateItem(item)).thenReturn(item);

        Item resultItem = stockService.updateItem(this.item);

        verify(stockRepository).updateItem(this.item);
        Assert.assertEquals(item.getId(), resultItem.getId());
    }

}
