package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.StockServiceImpl;

import java.util.Arrays;

public class GetAllItemsTest {

    @Test
    public void getAllItemsTest() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);
        Item item1 = new Item.Builder().setId("idTest1").setName("nameTest1").build();
        Item item2 = new Item.Builder().setId("idTest2").setName("nameTest2").build();

        Mockito.when(repositoryMock.getAllItems()).thenReturn(Arrays.asList(item1, item2));
        Assert.assertEquals(service.getAllItems(), Arrays.asList(item1, item2));

        Mockito.when(repositoryMock.getAllItems()).thenThrow(new BusinessLogicException("", "DATABASE_ERROR"));
        try {
            service.getAllItems();
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "DATABASE_ERROR");
        }
    }
}
