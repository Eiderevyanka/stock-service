package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.StockServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class GetAllItemsByCategory_test {


    /**
     * Checking whether the return value matches
     * Checking whether the method getAllItemsByCategory() is accessed
     */
    @Test
    public void getAllItemsByCategory_Test_Positive() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item());
        Mockito.when(repositoryMock.findAllItemsByCategory(Mockito.anyString())).thenReturn(itemList);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        List<Item> newItemList = service.getAllItemsByCategory(Mockito.anyString());

        Assert.assertNotNull(newItemList);
        Assert.assertEquals(itemList, newItemList);

        Mockito.verify(repositoryMock, Mockito.times(1)).findAllItemsByCategory(Mockito.anyString());
    }

    /**
     * checking the work of the method when category is null
     * Method getAllItemsByCategory() should throw a BusinessLogicException
     */
    @Test
    public void getAllItemsByCategory_Test_WhenCategoryIsNull() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        try {
            service.getAllItemsByCategory(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
        }

        Mockito.verify(repositoryMock, Mockito.times(0)).findAllItemsByCategory(Mockito.anyString());
    }

    /**
     * Checking for empty list return
     * Method getAllItemsByCategory() should throw a BusinessLogicException
     */
    @Test
    public void getAllItemsByCategory_Test_Return_Empty_List() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        List<Item> itemList = new ArrayList<>();
        Mockito.when(repositoryMock.findAllItemsByCategory(Mockito.anyString())).thenReturn(itemList);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        List <Item> newItemList = service.getAllItemsByCategory(Mockito.anyString());

        Assert.assertEquals(itemList, newItemList);

        Mockito.verify(repositoryMock, Mockito.times(1)).findAllItemsByCategory(Mockito.anyString());
    }
}
