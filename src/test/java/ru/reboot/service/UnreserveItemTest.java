package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.StockServiceImpl;

public class UnreserveItemTest {

    @Test
    public void unreserveItemTest_illegalArgument() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);
        Item itemMock = Mockito.mock(Item.class);

        try {
            service.unreserveItem(itemMock.getId(), 0);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
        }
    }

    @Test
    public void unreserveItemTest_databaseError_inFindItem() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        Mockito.when(repositoryMock.findItem(Mockito.anyString()))
                .thenThrow(new BusinessLogicException("", "DATABASE_ERROR"));
        try {
            service.unreserveItem(Mockito.anyString(), 5);
            Mockito.verify(repositoryMock).findItem(Mockito.anyString());
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "DATABASE_ERROR");
        }
    }

    @Test
    public void unreserveItemTest_databaseError_inUpdateItem() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);
        Item itemMock = Mockito.mock(Item.class);

        Mockito.when(itemMock.getReserved()).thenReturn(3);
        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(itemMock);
        Mockito.when(repositoryMock.updateItem(itemMock)).thenThrow(new BusinessLogicException("", "DATABASE_ERROR"));
        try {
            service.unreserveItem(Mockito.anyString(), 1);
            Mockito.verify(repositoryMock).findItem(Mockito.anyString());
            Mockito.verify(repositoryMock).updateItem(itemMock);
            Mockito.verify(itemMock).getReserved();
            Mockito.verify(itemMock).setReserved(2);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "DATABASE_ERROR");
        }
    }

    @Test
    public void unreserveItemTest_itemNotFound() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);
        Item itemMock = Mockito.mock(Item.class);

        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(null);
        try {
            service.unreserveItem("item123", 5);
            Mockito.verify(repositoryMock).findItem(Mockito.anyString());
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ITEM_NOT_FOUND");
        }
    }

    @Test
    public void unreserveItemTest_reservationError() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);
        Item itemMock = Mockito.mock(Item.class);

        Mockito.when(itemMock.getReserved()).thenReturn(20);
        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(itemMock);
        try {
            service.unreserveItem(Mockito.anyString(), 22);
            Mockito.verify(repositoryMock).findItem(Mockito.anyString());
            Mockito.verify(itemMock).getReserved();
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "RESERVATION_ERROR");
        }
    }

    @Test
    public void unreserveItemTest_positive() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);
        Item itemMock = Mockito.mock(Item.class);
        Item updatedItemMock = Mockito.mock(Item.class);

        Mockito.when(itemMock.getReserved()).thenReturn(10);
        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(itemMock);

        Mockito.when(updatedItemMock.getReserved()).thenReturn(7);
        Mockito.when(repositoryMock.updateItem(itemMock)).thenReturn(updatedItemMock);
        Item reservedItem = service.unreserveItem(Mockito.anyString(), 3);

        Assert.assertSame(updatedItemMock, reservedItem);
        Mockito.verify(repositoryMock).findItem(Mockito.anyString());
        Mockito.verify(repositoryMock).updateItem(Mockito.any());
        Mockito.verify(itemMock).getReserved();
        Mockito.verify(itemMock).setReserved(7);
    }
}
