package ru.reboot.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;
import ru.reboot.service.StockServiceImpl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CreateItemTest {

    private StockRepository stockRepository = Mockito.mock(StockRepository.class);
    private StockServiceImpl stockService = new StockServiceImpl();
    private Item item;

    @Before
    public void init() {
        stockService.setStockRepository(stockRepository);

        item = new Item();
        item.setId("01");
        item.setName("First Item");
    }

    @Test
    public void createItemIsAlreadyExists() {
        when(stockRepository.findItem(item.getId())).thenReturn(item);

        try {
            stockService.createItem(item);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCode.ITEM_ALREADY_EXISTS, e.getCode());
        }
    }

    @Test
    public void createItemContainsBadData() {
        try {
            stockService.createItem(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(ErrorCode.ILLEGAL_ARGUMENT, e.getCode());
        }
    }

    @Test
    public void createItemPositive() {
        when(stockRepository.findItem(item.getId())).thenReturn(null);
        when(stockRepository.createItem(item)).thenReturn(item);

        Item resultItem = stockService.createItem(this.item);

        verify(stockRepository).createItem(this.item);
        Assert.assertEquals(item.getId(), resultItem.getId());
    }

}
