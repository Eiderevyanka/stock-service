package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.StockServiceImpl;

import static org.mockito.Mockito.doThrow;

public class DeleteItem_test {

    /**
     * Checking whether the method deleteItem() is accessed
     */
    @Test
    public void deleteItem_Test_Positive() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        service.deleteItem("itemID");

        Mockito.verify(repositoryMock, Mockito.times(1)).deleteItem("itemID");

    }

    /**
     * Checking for null value itemId
     * Method deleteItem() should throw a BusinessLogicException
     */
    @Test
    public void deleteItem_Test_Negative_ItemId_isNull() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        try {
            service.deleteItem(null);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
            Assert.assertTrue(true);
        }

        Mockito.verify(repositoryMock, Mockito.times(0)).deleteItem("itemID");

    }

    /**
     * Checking method if item doesn't exist
     * Method deleteItem() should throw a BusinessLogicException
     */
    @Test
    public void deleteItem_Test_Negative_ItemId_isNotExists() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        doThrow(new BusinessLogicException("Item ID=" + "itemId" + "not found in DB", "ITEM_NOT_FOUND")).when(repositoryMock).deleteItem("itemId");

        try {
            service.deleteItem("itemId");
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ITEM_NOT_FOUND");
            Assert.assertTrue(true);
        }

        Mockito.verify(repositoryMock, Mockito.times(1)).deleteItem("itemId");
    }
}
