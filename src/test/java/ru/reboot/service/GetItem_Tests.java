package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.StockServiceImpl;

public class GetItem_Tests {


    /**
     * Checking whether the return value matches
     * Checking whether the method findItem() is accessed
     */
    @Test
    public void getItemPositive_Test() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);

        Item item = new Item.Builder().setId("id").setName("name").build();

        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(item);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        Item result = service.getItem(item.getId());

        Assert.assertNotNull(result);
        Assert.assertEquals(result, item);
        Assert.assertEquals(item.getName(), result.getName());
        Assert.assertEquals(item.getId(), result.getId());
        Mockito.verify(repositoryMock, Mockito.times(1)).findItem(Mockito.anyString());

    }

    /**
     * Checking for throwing an exception when the return value from the database is equal to null
     */
    @Test
    public void ifItemReturnNull_Test() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        try {
            Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(null);
            service.getItem("not null and not empty");
            Assert.fail();

        } catch (BusinessLogicException ex) {

            Assert.assertEquals(ex.getCode(), "ITEM_NOT_FOUND");

        }

    }

    /**
     * Checking for throwing an exception when the parameter value is empty
     */
    @Test
    public void ifItemIdReturnIsEmpty_Test() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        try {
            Mockito.when(service.getItem("")).thenThrow(BusinessLogicException.class);
            Assert.fail();

        } catch (BusinessLogicException ex) {

            Assert.assertEquals(ex.getCode(), "ILLEGAL_ARGUMENT");

        }

    }

    /**
     * Checking for throwing an exception when the parameter value is equal to null
     */
    @Test
    public void ifItemIdReturnNull_Test() {
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        try {
            Mockito.when(service.getItem(null)).thenThrow(BusinessLogicException.class);
            Assert.fail();

        } catch (BusinessLogicException ex) {

            Assert.assertEquals(ex.getCode(), "ILLEGAL_ARGUMENT");


        }
    }

}